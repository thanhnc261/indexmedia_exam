from django.conf.urls import patterns, include, url

from django.contrib import admin
from polls import views
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^polls/', include('polls.urls', namespace="polls"))
)
